import React from 'react';
import { graphql } from 'gatsby';
import styled from 'styled-components';
import { GatsbyImage, getImage } from 'gatsby-plugin-image';
import Layout from '../components/Layout';
import Typography from '@mui/material/Typography';
import Chip from '@mui/material/Chip';
import MaleIcon from '@mui/icons-material/Male';
import FemaleIcon from '@mui/icons-material/Female';
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
} from '@mui/material';
import { ContactButton } from '../components/ContactButton';

const Hero = styled.div`
  position: relative;
  margin-bottom: 1rem;
  height: 67vh;
  .hero-image {
    height: 100%;
    width: 100%;
    position: relative
  }
  .hero-overlay {
    position: absolute;
    padding: 1rem 4vw 2vw 4vw;
    bottom: 0;
    left: 0;
    width: 100%;
    background: linear-gradient(90deg, rgba(18,18,18,1) 10%, rgba(0,0,0,0) 100%);
    display: flex;
    flex-direction: row;
    justify-content: start;
    align-items: center;
    .gender-icon,
    .status-chip {
      margin-left: .8rem;
    }
  }
`

const Gallery = styled.div`
  display: grid;
  grid-template-columns: repeat(2, auto);
  grid-gap: 1rem;
  margin-bottom: 4rem;
  @media screen and (max-width: 900px) {
    grid-template-columns: 1fr;
  }
`

export default function Puppy({ data }) {
  const puppy = data.markdownRemark;
  const featuredImage = getImage(puppy.frontmatter.featured_image);
  const galleryImages = puppy.frontmatter.gallery_images;

  return (
    <Layout>
      <Hero>
        <GatsbyImage
          image={featuredImage}
          alt={`A photo of an American Pitbull puppy.`}
          className="hero-image"
        />
        <div className="hero-overlay">
          <Typography variant="h1">
            {puppy.frontmatter.title}
          </Typography>
          {puppy.frontmatter.sex === 'Female' ? (
            <FemaleIcon sx={{ fontSize: '4rem' }} color="secondary"/>
          ) : (
            <MaleIcon sx={{ fontSize: '4rem' }} color="primary"/>
          )}
          {puppy.frontmatter.available ? (
            <Chip className="status-chip" label="Available" color="success"/>
          ) : (
            <Chip className="status-chip" label="Not available"/>
          )}
        </div>
      </Hero>
      <TableContainer sx={{ maxWidth: 650 }} component={Paper}>
        <Table size="small">
          <TableHead>
            <TableRow>
              <TableCell>Sex</TableCell>
              <TableCell align="left">Age</TableCell>
              <TableCell align="left">Weight</TableCell>
              <TableCell align="left">Coat</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            <TableRow
              sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
            >
              <TableCell component="th" scope="row">
                {puppy.frontmatter.sex}
              </TableCell>
              <TableCell align="left">
                {puppy.frontmatter.age} wks
              </TableCell>
              <TableCell align="left">
                {puppy.frontmatter.weight} lbs
              </TableCell>
              <TableCell align="left">
                {puppy.frontmatter.coat_color}
              </TableCell>
            </TableRow>
          </TableBody>
        </Table>
      </TableContainer>
      <hr/>
      <div dangerouslySetInnerHTML={{ __html: puppy.html }} />
      <hr/>
      <Typography variant="h3">
        Gallery
      </Typography>
      <Gallery>
        {galleryImages.map((value, index) => {
          let image = getImage(value);
          return (
            <a key={index} href={value.publicURL}>
              <GatsbyImage
                key={index}
                image={image}
                alt={`A photo of an American Pitbull puppy.`}
              />
            </a>
          )
        })}
      </Gallery>
      {puppy.frontmatter.available &&
        <ContactButton
          sex={puppy.frontmatter.sex}
          puppyName={puppy.frontmatter.title}
        />
      }
    </Layout>
  )
}

export const query = graphql`
  query($slug: String!) {
    markdownRemark(fields: {slug: {eq: $slug}}) {
      id
      frontmatter {
        available
        coat_color
        birthdate(formatString: "MMMM DD, YYYY")
        age(difference: "weeks")
        sex
        title
        weight
        featured_image {
          childImageSharp {
            gatsbyImageData(
              width: 1200
              quality: 72
              jpgOptions: {progressive: true}
              placeholder: BLURRED
              formats: [AUTO, WEBP, AVIF]
            )
          }
        }
        gallery_images {
          publicURL
          childImageSharp {
            gatsbyImageData(
              aspectRatio: 1.5
              width: 800
              quality: 92
              jpgOptions: {progressive: true}
              placeholder: BLURRED
              formats: [AUTO, WEBP, AVIF]
            )
          }
        }
      }
      html
    }
  }
`
