import React from 'react';
import Layout from '../components/Layout';
import Seo from '../components/SEO';
import { Link } from 'gatsby';

export default function ErrorPage() {
  return (
    <Layout>
      <Seo title='404 Page Not Found' />
      <h1>NOT FOUND</h1>
      <p>You just hit a route that doesn&#39;t exist...</p>
      <p>Return <Link to='/'>home</Link>.</p>
    </Layout>
  )
}