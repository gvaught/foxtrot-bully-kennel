import React from "react"
import Layout from "../components/Layout"

import { Top } from '../components/sections/Top'
import { About } from '../components/sections/About'
import { Parents } from '../components/sections/Parents'
import { Puppies } from '../components/sections/Puppies'
import { Contact } from '../components/sections/Contact'

export default function Home({ location }) {
  return (
    <Layout>
      <Top />
      <About />
      <Parents />
      <Puppies />
      <Contact puppy={location.state && location.state.puppy} />
    </Layout>
  )
}
