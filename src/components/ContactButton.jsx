import React from 'react';
import Fab from '@mui/material/Fab';
import InfoIcon from '@mui/icons-material/Info';
import styled from 'styled-components';
import { Link } from 'gatsby';

const Wrapper = styled.div`
  position: fixed;
  z-index: 8;
  bottom: 4vw;
  right: 4vw;
  cursor: pointer;
  @media screen and (max-width: 800px) {
    right: 0;
    left: 4vw;
  }
`

export const ContactButton = (props) => {
  const color = props.sex === 'Female' ? 'secondary' : 'primary';

  return (
    <Wrapper>
      <Link to='/#contact' state={{ puppy: props.puppyName }}>
        <Fab
          // href="/#contact"
          color={color}
          variant="extended"
        >
          <InfoIcon sx={{ mr: 1 }}/> Request info
        </Fab>
      </Link>
    </Wrapper>
  )
}