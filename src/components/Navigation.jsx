import React from 'react';
import styled from 'styled-components';
import { StaticImage } from 'gatsby-plugin-image';

const NavWrapper = styled.div`
  max-width: 88vw;
  height: 5rem;
  margin: 2rem auto;
  display: flex;
  background-color: hsla(0, 0%, 40%, 0.40);
  border-radius: 24px;
  justify-content: start;
  align-items: center;
  backdrop-filter: blur(4px);
  width: auto;
  padding: .8rem;
  > a {
    padding: 2rem;
  }
  @media screen and (max-width: 800px) {
    margin: 0;
    padding: 4px;
    height: 80px;
    width: 80px;
    position: fixed;
    bottom: 8px;
    right: 8px;
    z-index: 10;
    display: block;
    background-color: hsla(0, 0%, 40%, 0.64);
    border-radius: 40px;
    .nav-logo {
      position: absolute;
      top: 1px;
      left: 3px;
    }
    > a {
      padding: 4px;
      display: block;
      position: relative;
    }
    > a.mobile-hidden {
      display: none;
    }
  }
`

export const Navigation = () => {
  return (
    <NavWrapper className="mainnavigation">
      <a href="/">
        <StaticImage
          className="nav-logo"
          src="./../images/foxtrotbullykennel--logo-notext.png"
          layout="fixed"
          alt="Foxtrot Bully Kennel Logo"
          height={64}
          aspectRatio={1/1}
          placeholder="tracedSvg"
          formats={["auto", "png", "webp"]}
        />
      </a>
      <a className="mobile-hidden" href="/#about">
        About
      </a>
      <a className="mobile-hidden" href="/#parents">
        Parents
      </a>
      <a className="mobile-hidden" href="/#puppies">
        Puppies
      </a>
      <a className="mobile-hidden" href="/#contact">
        Contact
      </a>
    </NavWrapper>
  )
}