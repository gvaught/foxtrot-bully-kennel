import React from "react"
import Theme from '../styles/theme'
import { Background } from './Background';
import { Navigation } from './Navigation';
import Container from './Container'
import { Footer } from './sections/Footer';
import Seo from './SEO';

export default function Layout({ location, children }) {
  return (
    <>
    <Seo />
      <Theme>
        <Background>
          <Navigation />
          <Container id='main-content'>
            {children}
          </Container>
          <Footer />
        </Background>
      </Theme>
    </>
  )
}