import React from 'react';
import styled from 'styled-components';
import { StaticImage } from 'gatsby-plugin-image';

const StyledBg = styled.div` 
  display: grid;
  width: 100vw;
  background-size: cover;
  &>.background-image {
    grid-area: 1/1;
    z-index: -1;
    height: 100vh;
  }
  &>.children-wrapper {
    background-image: linear-gradient(to bottom, rgba(18,18,18,0.4) 0%, rgba(18,18,18,1) 100%);
    grid-area: 1/1;
    background-size: cover;
    height: 100vh;
  }
`

export const Background = ({children}) => {
  return (
    <StyledBg>
      <StaticImage
        className="background-image"
        layout="fullWidth"
        aspectRatio={2 / 1}
        alt="Background image"
        src={"./../images/bg.jpg"}
        formats={["auto", "webp", "avif"]}
      />
      <div className="children-wrapper">
        {children}
      </div>
    </StyledBg>
  )
}