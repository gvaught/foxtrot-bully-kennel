import React from 'react';
import Typography from '@mui/material/Typography';

export const Parents = () => {
  return (
    <section id="parents">
      <Typography variant="h2">Parents</Typography>
      <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce id nunc luctus risus varius dictum eget eu neque. Suspendisse malesuada nunc in libero molestie, et finibus lacus efficitur. Morbi convallis velit id diam tempor porta. 
      </p>
    </section>
  )
}