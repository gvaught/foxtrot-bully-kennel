import React from 'react';
import Typography from '@mui/material/Typography';

export const About = () => {
  return (
    <section id="about">
      <Typography variant="h2">About</Typography>
      <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce id nunc luctus risus varius dictum eget eu neque. Suspendisse malesuada nunc in libero molestie, et finibus lacus efficitur. Morbi convallis velit id diam tempor porta. Cras varius vel tortor euismod finibus. Nullam mattis, erat vel porta lobortis, neque risus rhoncus mi, sit amet  sollicitudin dui metus sit amet velit. In hac habitasse platea dictumst. Proin id ipsum quis nunc laoreet dignissim. Nam placerat ligula non euismod aliquam. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Pellentesque pulvinar ac tellus at pharetra. Maecenas lobortis velit in vestibulum rhoncus. Nam commodo, justo a cursus dictum, nulla purus rhoncus massa, nec hendrerit eros risus ac nisl. Etiam consectetur fringilla auctor.
      </p>
    </section>
  )
}