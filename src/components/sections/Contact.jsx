import React from 'react';
import { graphql, useStaticQuery } from 'gatsby';
import { 
  Grid, 
  TextField, 
  Autocomplete, 
  InputAdornment,
  Button,
  Box,
  Link,
} from '@mui/material';
import SendIcon from '@mui/icons-material/Send';
import InstagramIcon from '@mui/icons-material/Instagram';
import PhoneOutlinedIcon from '@mui/icons-material/PhoneOutlined';
import EmailOutlinedIcon from '@mui/icons-material/EmailOutlined';
import Typography from '@mui/material/Typography';
import styled from 'styled-components';

const StyledSection = styled.section`
  margin: 1rem 0;
`;

export const Contact = (props) => {

  const data = useStaticQuery(graphql`
    query {
      allMarkdownRemark {
        nodes {
          id
          frontmatter {
            title
          }
        }
      }
    }
  `);
  const puppies = data.allMarkdownRemark.nodes.map(node => node.frontmatter.title);

  return (
    <StyledSection id='contact'>
      <Typography variant='h2'>Contact</Typography>
      <Typography variant='body' color="primary">
        <Link href="https://www.instagram.com/foxtrot_bully_kennel/">
          <Box display="flex" flexDirection="row" alignItems="center">
            <InstagramIcon/>&nbsp;foxtrot_bully_kennel
          </Box>
        </Link>
        <Link tel="+12145555555">
          <Box display="flex" flexDirection="row" alignItems="center">
            <PhoneOutlinedIcon/>&nbsp;+1 (214) 555-5555
          </Box>
        </Link>
        <Link mailto="foxtrotbullykennel@gmail.com">
          <Box display="flex" flexDirection="row" alignItems="center">
            <EmailOutlinedIcon/>&nbsp;foxtrotbullykennel@gmail.com
          </Box>
        </Link>
      </Typography>
      <Typography variant='body'>
        ... Or send us an email directly here:
      </Typography>
      <form name="contact" method="POST" data-netlify="true">
        <Grid container spacing={2} sx={{ mt: 0.5 }} maxWidth="md" autoComplete="on">
          <Grid item xs={12} sm={6} md={6}>
            <TextField
              fullWidth
              type='text'
              name='firstName'
              label='First name'
              autoComplete="given-name"
              variant='outlined'
              required
            />
          </Grid>
          <Grid item xs={12} sm={6} md={6}>
            <TextField
              fullWidth
              type='text'
              name='lastName'
              label='Last name'
              autoComplete="family-name"
              variant='outlined'
              required
            />
          </Grid>
          <Grid item xs={12} sm={6} md={6}>
            <TextField
              fullWidth
              type='email'
              name='email'
              label='Email address'
              autoComplete="email"
              variant='outlined'
              required
            />
          </Grid>
          <Grid item xs={12} sm={6} md={6}>
            <TextField
              fullWidth
              type='phone'
              name='phone'
              label='Phone number'
              autoComplete="home tel"
              variant='outlined'
            />
          </Grid>
          <Grid item xs={12} sm={12} md={12}>
            <Autocomplete
              fullWidth
              multiple
              name="puppy name(s)"
              options={puppies}
              getOptionLabel={(option) => option}
              defaultValue={props.puppy ? [props.puppy] : []}
              filterSelectedOptions
              renderInput={(params) => (
                <TextField
                  {...params}
                  fullWidth
                  label="Name of puppy/puppies"
                />
              )}
            />
          </Grid>
          <Grid item  xs={12} sm={6} md={6}>
            <TextField
              fullWidth
              type='text'
              name='twitter'
              label='Twitter'
              variant='outlined'
              autoComplete='nickname'
              InputProps={{
                startAdornment: <InputAdornment position="start">@</InputAdornment>,
              }}
            />
          </Grid>
          <Grid item  xs={12} sm={6} md={6}>
            <TextField
              fullWidth
              type='text'
              name='instagram'
              label='Instagram'
              variant='outlined'
              autoComplete='nickname'
            />
          </Grid>
          <Grid item  xs={12} sm={12} md={12}>
            <TextField
              fullWidth
              type='text'
              name='message'
              multiline
              minRows={2}
              label='Message'
              variant='outlined'
            />
          </Grid>
          <Grid item justifyContent="flex-end">
            <Button 
              type='submit' 
              variant='contained' 
              endIcon={<SendIcon />}
            >
              Submit
            </Button>
          </Grid>
        </Grid>
      </form>
    </StyledSection>
  );
};
