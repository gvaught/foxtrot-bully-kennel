import React from 'react';
import { StaticImage } from 'gatsby-plugin-image';
import { Typography } from '@mui/material';
import styled from 'styled-components';

const StyledSection = styled.section`
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-gap: 0 4rem;
  margin: 2rem auto;
  max-width: 64vw;
  align-items: center;
  justify-content: center;
  @media screen and (max-width: 800px) {
    max-width: 90vw;
  }
  & > .heading {
    max-width: 18rem;
    @media screen and (min-width: 600px) {
      @media screen and (max-width: 800px) {
        font-size: 4rem;
      }
    }
  }
  & > h4 {
    grid-column: 2 / -1;
  }
`

export const Top = () => {
  return (
    <StyledSection id="top">
      <StaticImage
        className="main-logo"
        src="./../../images/foxtrotbullies-logo.png"
        layout="constrained"
        alt="Foxtrot Bully Kennel Logo."
        aspectRatio={1/1}
        placeholder="tracedSvg"
        formats={["auto", "png", "webp"]}
      />
      <Typography variant='h1' className="heading">
        Foxtrot Bully Kennel
      </Typography>
      <Typography variant="h4">
        Cleburne, TX
      </Typography>
    </StyledSection>
  );
}