import React from 'react';
import styled from 'styled-components';
import { Typography, Link } from '@mui/material';

const StyledSection = styled.section`
  margin: 2rem auto;
  max-width: 90vw;
  text-align: center;
`


export const Footer = () => {
  return (
    <StyledSection>
      <Typography variant="overline">
        &copy; { new Date().getFullYear() } <Link href="https://gavinvaught.com/">Gavin Vaught</Link>
      </Typography>
    </StyledSection>
  )
}