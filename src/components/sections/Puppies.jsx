import React from 'react';
import { graphql, Link, useStaticQuery } from 'gatsby';
import { GatsbyImage, getImage } from 'gatsby-plugin-image';
import Typography from '@mui/material/Typography';
import styled from 'styled-components';

const StyledSection = styled.section`

`

const CardGroup = styled.div`
  display: grid;
  grid-template-columns: repeat(2, 1fr);
  grid-gap: 1rem;
`

const LinkCard = styled(Link)`
  position: relative;
  display: inline-grid;
  transition: all 400ms;
  > .image-desc-wrapper {
    position: absolute;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    color: white;
    visibility: hidden;
    opacity: 0;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    transition: opacity .2s, visibility .2s;
    .image-desc {
      transition: .2s;
      transform: translateY(1em);
      text-align: center;
      z-index: 2;
    }
  }
  :hover {
    > .image {
      filter: brightness(0.4) blur(4px);
    }
    & > .image-desc-wrapper {
      visibility: visible;
      opacity: 1;
      .image-desc {
        transform: translateY(0);
      }
    }
  }
`

export const Puppies = () => {
  const data = useStaticQuery(graphql`
    query {
      allMarkdownRemark {
        nodes {
          id
          frontmatter {
            title
            birthdate(formatString: "MMMM DD, YYYY")
            age(difference: "weeks")
            sex
            featured_image {
              childImageSharp {
                gatsbyImageData(
                  width: 500
                  placeholder: BLURRED
                  formats: [AUTO, WEBP, AVIF]
                )
              }
            }
          }
          fields {
            slug
          }
        }
      }
    }
  `);

  return (
    <StyledSection id="puppies">
      <Typography variant="h2">Puppies</Typography>
      <CardGroup>
        {data.allMarkdownRemark.nodes.map(node => {
          const image = getImage(node.frontmatter.featured_image);
          return (
            <LinkCard key={node.id.toString()} to={node.fields.slug}>
              <GatsbyImage
                className="image"
                alt="An image of a puppy"
                image={image}
                key={node.id.toString()}
              />
              <div className="image-desc-wrapper">
                <p className="image-desc">{node.frontmatter.title}</p>
                <p className="image-desc">{node.frontmatter.birthdate}</p>
                <p className="image-desc">{node.frontmatter.sex}</p>
              </div>
            </LinkCard>
          );
        })}
      </CardGroup>
    </StyledSection>
  )
}