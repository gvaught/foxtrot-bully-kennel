import React from 'react'
import PropTypes from 'prop-types'
import { createGlobalStyle } from 'styled-components'
import { 
  createTheme,
  ThemeProvider,
  responsiveFontSizes 
} from '@mui/material/styles';
import CssBaseline from '@mui/material/CssBaseline';
import './styles.css'

let themeOverrides = createTheme({
  palette: {
    mode: 'dark',
  },
  typography: {
    h1: {
      fontFamily: [
        '"Vollkorn"',
        'serif',
      ].join(','),
    },
    h2: {
      fontFamily: [
        '"Vollkorn"',
        'serif',
      ].join(','),
    },
    h3: {
      fontFamily: [
        '"Vollkorn"',
        'serif',
      ].join(',')
    },
    h4: {
      fontFamily: [
        '"Vollkorn"',
        'serif',
      ].join(','),
    },
  },
});

export const theme = responsiveFontSizes(themeOverrides);

export const GlobalStyle = createGlobalStyle`
  body {
    font-family: var(--font-family-sans-serif);
    background-color: var(--dark);
    color: var(--light);
    width: 100%;
    height: auto;
    max-width: 100vw;
    margin: 0;
    padding: 0;
    overflow-x: hidden;
    overflow-y: scroll;
  }
  * {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
  }
  #___gatsby {
    width: 100%;
    height: 100%;
  }
  h1,
  h2,
  h3 {
    margin: .4rem 0;
  }
  p {
    margin-bottom: 1rem;
  }
  h1.heading {
    font-weight: 900;
    letter-spacing: -0.3rem;
  }
  h2.subheading {
    text-align: start;
    font-family: var(--font-family-display);
    font-size: 3rem;
    letter-spacing: 0;
    margin: 0 0 .4rem 0;
    font-variation-settings: 
      "wght" 900;
  }
  hr {
    margin: 1rem 0;
  }
  .mainnavigation {
    > a {
      color: var(--gray);
      font-variant: small-caps;
      text-transform: lowercase;
      font-weight: 500;
      text-decoration: none;
      transition: all 256ms;
      font-size: 1.1rem;
      :hover {
        text-decoration: underline 1px dotted;
        color: var(--light);
      }
    }
  }
`

export default function Theme(props) {
  return (
    <>
      <GlobalStyle />
      <ThemeProvider theme={theme}>
        <CssBaseline />
        {props.children}
      </ThemeProvider>
    </>
  )
}

Theme.propTypes = {
  children: PropTypes.node,
}
