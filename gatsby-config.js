module.exports = {
  siteMetadata: {
    title: `Foxtrot Bully Kennel`,
    titleTemplate: "%s",
    description: ``,
    siteUrl: `https://foxtrotbullykennel.com`,
    image: `./src/images/foxtrotbullykennel--logo.png`,
  },
  plugins: [
    {
      resolve: "gatsby-source-filesystem",
      options: {
        name: `puppies`,
        path: `${__dirname}/puppies`,
      }
    },
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-transformer-remark`,
      options: {
        plugins: [
          {
            resolve: `gatsby-remark-images`,
            options: {
              maxWidth: 800,
            },
          },
          `gatsby-remark-autolink-headers`,
        ],
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-remark-images`,
    `gatsby-plugin-image`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: "Foxtrot Bully Kennel",
        short_name: "Foxtrot Bully Kennel",
        start_url: "/",
        background_color: "#888888",
        theme_color: "#888888",
        display: "standalone",
        icon: "./src/images/foxtrotbullykennel--logo.png",
        crossOrigin: `use-credentials`,
      },
    },
    {
      resolve: `gatsby-plugin-styled-components`,
      options: {

      },
    },
    `gatsby-plugin-react-helmet`
  ],
}
