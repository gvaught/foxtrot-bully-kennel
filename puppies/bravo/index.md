---
title: Bravo
featured_image: IMG_0300.jpeg
sex: Male
birthdate: 2021-09-30
age: 2021-09-30
weight: 0
coat_color: White
available: true
gallery_images: [
  IMG_0010.jpeg,
  IMG_0022.jpeg,
  IMG_9529.jpeg,
  IMG_9535.jpeg,
  IMG_9537.jpeg,
]
---
