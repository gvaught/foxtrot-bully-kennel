---
title: Papa
featured_image: IMG_0328.jpeg
sex: Male
birthdate: 2021-09-30
age: 2021-09-30
weight: 0
coat_color: White
available: true
gallery_images: [
  IMG_0026.jpeg,
  IMG_0040.jpeg,
  IMG_0049.jpeg,
  IMG_9456.jpeg,
  IMG_9466.jpeg,
  IMG_9471.jpeg,
]
---
