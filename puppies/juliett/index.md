---
title: Juliett
featured_image: IMG_0229.jpeg
sex: Female
birthdate: 2021-09-30
age: 2021-09-30
weight: 0
coat_color: White
available: true
gallery_images: [
  IMG_0188.jpeg,
  IMG_0212.jpeg,
  IMG_0220.jpeg,
  IMG_9387.jpeg,
  IMG_9395.jpeg,
  IMG_9396.jpeg,
]
---
