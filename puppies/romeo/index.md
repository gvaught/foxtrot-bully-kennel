---
title: Romeo
featured_image: IMG_0177.jpeg
sex: Male
birthdate: 2021-09-30
age: 2021-09-30
weight: 0
coat_color: White
available: true
gallery_images: [
  IMG_0143.jpeg,
  IMG_0159.jpeg,
  IMG_0166.jpeg,
  IMG_9364.jpeg,
  IMG_9375.jpeg,
  IMG_9377.jpeg,
]
---