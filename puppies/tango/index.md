---
title: Tango
featured_image: IMG_0282.jpeg
sex: Female
birthdate: 2021-09-30
age: 2021-09-30
weight: 0
coat_color: White
available: false
gallery_images: [
  IMG_0074.jpeg,
  IMG_0078.jpeg,
  IMG_9483.jpeg,
  IMG_9490.jpeg,
  IMG_9493.jpeg,
]
---

