---
title: Charlie
featured_image: IMG_0270.jpeg
sex: Female
birthdate: 2021-09-30
age: 2021-09-30
weight: 0
coat_color: White
available: true
gallery_images: [
  IMG_0247.jpeg,
  IMG_0256.jpeg,
  IMG_9518.jpeg,
  IMG_9519.jpeg,
  IMG_9525.jpeg,
]
---

----

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce id nunc luctus risus varius dictum eget eu neque. Suspendisse malesuada nunc in libero molestie, et finibus lacus efficitur. Morbi convallis velit id diam tempor porta.

Cras varius vel tortor euismod finibus. Nullam mattis, erat vel porta lobortis, neque risus rhoncus mi, sit amet  sollicitudin dui metus sit amet velit.
