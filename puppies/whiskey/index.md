---
title: Whiskey
featured_image: IMG_0296.jpeg
sex: Male
birthdate: 2021-09-30
age: 2021-09-30
weight: 0
coat_color: White
available: true
gallery_images: [
  IMG_0114.jpeg,
  IMG_0127.jpeg,
  IMG_0130.jpeg,
  IMG_9414.jpeg,
  IMG_9435.jpeg,
  IMG_9446.jpeg,
]
---
